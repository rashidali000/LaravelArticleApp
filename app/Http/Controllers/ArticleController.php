<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Form;
use Illuminate\Support\Facades\Auth;
use Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['only'=>'create']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = \App\Article::paginate(5);
            return view('articles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = Auth::user();
        $all_inputs = Request::all();
        $user->articles()->create($all_inputs);
        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param $article
     * @return Response
     * @internal param int $id
     */
    public function show($article)
    {
        return view ('articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
