<?php
use Illuminate\Database\Seeder;
class ArticleTableSeeder extends seeder
{
    public function run()
    {
        factory('App\Article',50)->create(['user_id'=>rand(53,300)]);
    }
}