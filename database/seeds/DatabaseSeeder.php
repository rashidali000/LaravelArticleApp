<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    protected $toTruncate = ['users','articles'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

//        factory('App\User',50)->create(
//            ['password'=>bcrypt('password')]
//        );
//        factory('App\Article',200)->create(
//
//        );
//        DB::table('users')->truncate();
//        $this->call('UserTableSeeder');
        $this->call('ArticleTableSeeder');

        Model::reguard();
    }
}
