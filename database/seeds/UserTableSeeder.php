<?php
use Illuminate\Database\Seeder;
class UserTableSeeder extends seeder
{
    public function run()
    {
        factory('App\User',50)->create();
    }
}