@extends('app')
@section('content')
    @foreach($articles as $article)
        <article>
            <a href="{{ action('ArticleController@show',[$article->id])  }}"><h1>{{ $article->title }}</h1></a>
            <a href="{{ action('ArticleController@edit',[$article->id])  }}"><i class="fa fa-edit"></i></a>
            <a style="color: red;" href="{{ action('ArticleController@destroy',[$article->id])  }}"><i class="fa fa-remove"></i></a>
        </article>
    @endforeach
    {!! $articles->render() !!}
@endsection