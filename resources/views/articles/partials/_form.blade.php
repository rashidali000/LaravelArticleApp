{!! @Form::open(['url'=>'articles']) !!}
<!-- Title: input field -->
<div class="form-group">
    {!! Form::label('title:','Title:') !!}
    {!! Form::text('title',null,['class'=>'form-control']) !!}
</div>
<!-- Descriptoin input field -->
<div class="form-group">
    {!! Form::label('Descriptoin','Descriptoin') !!}
    {!! Form::textarea('body',null,['class'=>'form-control']) !!}
</div>
<!-- Published Date: input field -->
<div class="form-group">
    {!! Form::label('Published Date:','Published Date:') !!}
    {!! Form::input('date','published_at',date('Y-m-d'),['class'=>'form-control']) !!}
</div>
<!--  input field -->
<div class="form-group">
    {!! Form::submit('Submit',null,['class'=>'form-control btn btn-primary']) !!}
</div>
{!! @Form::close() !!}

