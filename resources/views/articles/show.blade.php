@extends('app')
@section('content')
    <h1>{{$article->title}}</h1>
    <p>{{$article->body}}</p>
    <a href="#">{{$article->user->name}}</a>
@endsection