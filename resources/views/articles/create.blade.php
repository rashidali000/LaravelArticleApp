@extends('app')
    @section('content')
        <h1>Write a new Article</h1>
        @include('articles.partials._form')
        @endsection